Mezzanine Modern Business
==================

Integration template Modern Business with mezzanine

Features
--------

 - Integrate template Modern Business (http://startbootstrap.com/modern-business) with mezzanine


How to Use
----------

 1. Get and install the package:

        pip install mezzanine-modern-business

    or

        git clone git@bitbucket.org:naritas/mezzanine-modern-business.git mezzanine-modern-business

        cd mezzanine-modern-business

        python setup.py install

    Mezzanine 3 or higher is required.

 2. Install the app in your Mezzanine project by adding
    `mezzanine_modern_business` first on the  list of `INSTALLED_APPS` in your
    project's `settings.py`.

 3. Run:

        python manage.py migrate
    or
        python manage.py syncdb

 3. Add 'MODERN_BUSINESS_LOGO' to TEMPLATE_ACCESSIBLE_SETTINGS.

    Example:

        TEMPLATE_ACCESSIBLE_SETTINGS = ('ACCOUNTS_VERIFICATION_REQUIRED', 'BITLY_ACCESS_TOKEN', 'BLOG_USE_FEATURED_IMAGE',
                                        'COMMENTS_DISQUS_SHORTNAME', 'COMMENTS_NUM_LATEST', 'COMMENTS_DISQUS_API_PUBLIC_KEY',
                                        'COMMENTS_DISQUS_API_SECRET_KEY', 'COMMENTS_USE_RATINGS', 'DEV_SERVER', 'FORMS_USE_HTML5',
                                        'GRAPPELLI_INSTALLED', 'GOOGLE_ANALYTICS_ID', 'JQUERY_FILENAME', 'LOGIN_URL',
                                        'LOGOUT_URL', 'SITE_TITLE', 'SITE_TAGLINE', 'MODERN_BUSINESS_LOGO')

 4. Configure settings: MODERN_BUSINESS_LOGO.

License
-------

Licence: BSD. See included `LICENSE` file.

Note that this license applies to this repository only.