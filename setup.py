#!/usr/bin/env python

from setuptools import setup, find_packages

import os

if os.path.exists("README.txt"):
    readme = open("README.txt")
else:
    readme = open("README.md")

setup(
    name="mezzanine-firenze-template",
    version="0.8.3",
    description="Integration template firenze template with mezzanine",
    long_description=readme.read(),
    author="Luis Velez",
    author_email="lvelezsantos@gmail.com",
    license="BSD",
    url="",
    install_requires=(
        "mezzanine==4.2.2",
    ),
    packages=['mezzanine_firenze_template'],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Framework :: Django",
    ],
)
