# coding=utf-8
from django.utils.translation import ugettext_lazy as _
from mezzanine.conf import register_setting

register_setting(
    name="FIRENZE_ADDRESS",
    description=_(u"Dirección"),
    label=_(u"Dirección"),
    editable=True,
    default=u'Pueblo Nuevo, Cl. 16 # 10 - 206 - Av. Colombia, Magangué - Colombia',
)

register_setting(
    name="FIRENZE_PHONE",
    description=_(u"Telefono"),
    label=_(u"Telefono"),
    editable=True,
    default='+57 310 410 1969',
)

register_setting(
    name="FIRENZE_EMAIL",
    description=_(u"Correo Electronico"),
    label=_(u"Correo Electronico"),
    editable=True,
    default='hotelfirenze_r@hotmail.com',
)