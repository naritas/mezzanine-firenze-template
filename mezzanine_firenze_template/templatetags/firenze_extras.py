from django import template

register = template.Library()

@register.filter
def add_class(field, css):
    """
    """
    try:
        attrs = field.form.fields.get(field.name).widget.attrs
        classess = (attrs['class'] + ' ' if 'class' in attrs.keys() else '') + css

        attrs['class'] = classess
        return field.as_widget(attrs=attrs)
    except AttributeError:  # not a form field
        return field


@register.filter
def add_placeholder(field, placeholder):
    """
    """

    try:
        attrs = field.form.fields.get(field.name).widget.attrs
        attrs['placeholder'] = placeholder
        attrs['class'] = 'form-control'
        return field.as_widget(attrs=attrs)
    except AttributeError:  # not a form field
        return field
